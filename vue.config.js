const endpoint = 'http://recruitment.poseidon.gwf.ch/simulator/api.php'

module.exports = {
  devServer: {
    proxy: {
      '^/simulator': {
        target: endpoint,
        ws: true,
        changeOrigin: true
      }
    }
  }
}