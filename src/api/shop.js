import VueResource from 'vue-resource'
import Vue from 'vue'

Vue.use(VueResource)

Vue.http.options.emulateHTTP = true
Vue.http.options.emulateJSON = true

const endpoint = 'http://recruitment.poseidon.gwf.ch/simulator/api.php'

export default {
    getAsync (options, cb_success, cb_error) {
        Vue.http.get(endpoint, options)
        .then((response) => {
            cb_success(response.body)
        }, (response) => {
            cb_error(response)
        })
    },
}
