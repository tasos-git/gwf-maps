import shop from '../../api/shop'
import * as types from '../mutation-types'
import Vue from 'vue'

const state = {
	meters: [],
	dataStore: { type: 'FeatureCollection', features: [] },
	map: {
		accessToken: 'pk.eyJ1IjoidGFzb3MtZyIsImEiOiJjanNseHMwdHQxa2ltNDRubXJoZDRhdWhlIn0.9Ks_YpRIt-nU0y3bIFzFfw',
		mapStyle: 'mapbox://styles/mapbox/streets-v10',
		mapMeter: {}
	},
	processing: false
}

// getters
const getters = {
	meters: state => state.meters,
	dataStore: state => state.dataStore,
	processing: state => state.processing,
	accessToken: state => state.map.accessToken,
	mapStyle: state => state.map.mapStyle,
	mapMeter: state => state.map.mapMeter,
}

// actions
const actions = {

    getMeters ({ commit, state }, payload) {
    	commit(types.PROCESSING_START)
    	return new Promise((resolve, reject) => {
            shop.getAsync({ params: {
			    		action: 'getMeters'
		    		}
		    	},
		        data => {
		        	commit(types.PROCESSING_STOP)
		            commit(types.METERS_SET, data)
		            resolve(data)
		        },
		        err => {
		        	commit(types.PROCESSING_STOP)
		            reject(err)
		        }
		    )
        })
    },

	getReadOut ({ commit, state }, payload) {
    	return new Promise((resolve, reject) => {
            shop.getAsync({ params: {
			    		action: 'getReadOut',
			    		meterSerialNo: payload.serialNo,
			    		key: payload.key,
			    		version: 2
		    		}
		    	},
		        data => {
		        	commit(types.MAP_METER, data)
		        	resolve(data)
		        },
		        err => {
		        	reject(err)
		        }
	    	)
        })
	},
}

// mutations
const mutations = {
	[types.METERS_SET] (state, result) {
		state.meters = [...result]
		state.dataStore.features = state.meters.map(feature => {
			return {
				type: 'Feature',
				geometry: {
					type: 'Point',
					coordinates: [
						feature.lat,
						feature.long
					]
				},
				properties: {
					serialNo: feature.serialNo,
					key: feature.key,
					lat: feature.lat,
					long: feature.long,
				}
			}
		})
	},
	[types.MAP_METER] (state, payload) {
		state.map.mapMeter = Object.assign({}, payload)
	},
	[types.PROCESSING_START] (state) {
	    state.processing = true
	},
	[types.PROCESSING_STOP] (state) {
	    state.processing = false
	},

}

export default {
	state,
	getters,
	actions,
	mutations
}
