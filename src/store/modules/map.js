import Vue from 'vue'
import * as types from '../mutation-types'

const state = {
	map: {},
	
	selectedMarker: {}
}

// getters
const getters = {
	accessToken: state => state.accessToken,
	mapStyle: state => state.mapStyle,
	selectedMarker: state => state.selectedMarker,
}

// actions
const actions = {
	initMap ({ commit, state }, payload) {
		commit(types.MAP_SET, payload)
	},

	mapMeter ({ commit, state }, payload) {
    	//console.log(payload, state.map)
    },

    flyToMarker() {

    },
    
}

// mutations
const mutations = {
	[types.MAP_SET] (state, map) {
		state.map = map
	}
}

export default {
	state,
	getters,
	actions,
	mutations
}
