import Vue from 'vue'
import Vuex from 'vuex'
import meters from './store/modules/meters.js'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
	modules: {
		meters
	},
	strict: debug
})
