import Vue from 'vue'
import Router from 'vue-router'
import Overview from './views/Overview.vue'

Vue.use(Router)

export default new Router({
    routes: [
        { path: '/', redirect: { name: 'overview' } },
        {
            path: '/meters',
            name: 'overview',
            component: Overview
        },
        {
            path: '*',
            name: 'not-found',
            component: () => import(/* webpackChunkName: "not-found" */ './views/NotFound.vue')
        }
    ]
})